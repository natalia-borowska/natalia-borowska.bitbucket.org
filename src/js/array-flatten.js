class FlattenArray {
	constructor(contentListItem, paragTop, paragBottom) {
		this.contentListItem = contentListItem;
		this.paragTop = paragTop;
		this.paragBottom = paragBottom;
	}
	flattenArr(arr) {
		return [].concat(...arr);	
	}
	deepFlattenArray(arr) {
		return this.flattenArr(
			arr.map((elem) => {
				return Array.isArray(elem) ? this.deepFlattenArray(elem) : elem;
			})
		)
	}
	init() {
		
		const contentListItems = document.querySelectorAll(this.contentListItem);
		
			contentListItems.forEach((element, index) => {
				const paragraphTop = element.querySelector(this.paragTop);
				const paragraphBottom = element.querySelector(this.paragBottom);
				const arrayToFlatten = paragraphTop.querySelector('span').textContent;
				const flattenedArray = this.deepFlattenArray(eval(arrayToFlatten));
				paragraphBottom.querySelector('span').textContent = `[${flattenedArray}]`;
				
			});			
		
	}
}

export default new FlattenArray('.content-list-item', '.parag-top', '.parag-bottom');