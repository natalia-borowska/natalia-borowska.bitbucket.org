class ColorActiveLink {
	constructor(mainHeader, navListItem) {
		this.mainHeader = mainHeader;
		this.navListItem = navListItem;
	}
	init() {
		const header = document.querySelector(this.mainHeader);
		const navListItems = header.querySelectorAll(this.navListItem);
		const path = window.location.pathname;
		
		navListItems.forEach((elem, index) => {
			const link = elem.querySelector('a');
			const linkHref = link.getAttribute('href');

			if(path !== '/' && path !== '/dist/') {
				link.classList.remove('is-active');
			}
			if(path.indexOf(linkHref) !== -1) {
				link.classList.add('is-active');
			}

		});
	}
}

export default new ColorActiveLink('.main-header', '.nav-list-item');