class Slider {
	constructor(itemWrapper, item) {
		this.itemWrapper = itemWrapper;
		this.item = item;
	}
	init() {

		const mobileViewport = window.matchMedia("screen and (max-width: 480px)");
		const slider = document.querySelector(this.itemWrapper);
		const that = this;

		function runSlider(itemWrapper, item){

			const slider = document.querySelector(itemWrapper);
			const slide = slider.querySelectorAll(item);
			const num = slide.length;

			// edge doesn't work when we apply calc with transitions
			if(window.navigator.userAgent.indexOf("Edge") <= -1) {
				slider.classList.add('is-transitioning');
			}

			slider.style.setProperty('--n', num);
			let x1 = null;
			let x2 = null;
			let i = 0;

			const move = function(e) {
				if(x1 || x1 === 0) {
					if(e.type === 'touchend') {
						let rect = e.target.getBoundingClientRect();
						x2 = e.changedTouches[0].pageX - rect.left;
					}
					else {
						x2 = e.offsetX;
					}
					
					let distanceX = x2 - x1;
					let sign = Math.sign(distanceX);

					if((i > 0 || sign < 0) && (i < num-1 || sign > 0)){
						i -= sign;
						slider.style.setProperty('--i', i);
						x1 = null;
					}
				}
			}

			const lock = function(e) {
				if(e.type === 'touchstart') {
					let rect = e.target.getBoundingClientRect();
					x1 = e.targetTouches[0].pageX - rect.left;
				}
				else {
					x1 = e.offsetX;
				}
			}

			slider.addEventListener('mousedown', (e) => lock(e), false);
			slider.addEventListener('touchstart', (e) => lock(e), false);
			slider.addEventListener('mouseup', (e) => move(e), false);
			slider.addEventListener('touchend', (e) => move(e), false);
			slider.addEventListener('touchmove', e => {e.preventDefault()}, false);
		}

		function terminateSlider(itemWrapper){
			const slider = document.querySelector(itemWrapper);
			slider.removeEventListener('mousedown', (e) => {e.preventDefault()}, false);
			slider.removeEventListener('touchstart', (e) => {e.preventDefault()}, false);
			slider.removeEventListener('mouseup', (e) => {e.preventDefault()}, false);
			slider.removeEventListener('touchend', (e) => {e.preventDefault()}, false);
		}

		if(document.body.contains(slider)) {

			if(mobileViewport.matches) {
				
				slider.classList.add('slider-on');
				runSlider(this.itemWrapper, this.item);

			}
			else {
				
				if(slider.classList.contains('slider-on')) {
					slider.classList.remove('slider-on');
				}
				terminateSlider(this.itemWrapper);
			}

			mobileViewport.addListener(function(mv) {
				if(mv.matches) {
					runSlider(that.itemWrapper, that.item);
					slider.classList.add('slider-on');
				}
				else {
					if(slider.classList.contains('slider-on')) {
						slider.classList.remove('slider-on');
					}
					terminateSlider(that.itemWrapper);

					return;
				}
			});
		}
	}
}

export default new Slider('.item-wrapper', '.item');