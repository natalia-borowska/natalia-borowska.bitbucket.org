import style from "./_scss/main.scss";
import slider from './js/slider.js';
import flattenArray from './js/array-flatten.js';
import colorActiveLink from './js/color-active-link.js';

document.addEventListener('DOMContentLoaded', () => {
	colorActiveLink.init();
});

window.addEventListener('load', () => {
	slider.init();
	flattenArray.init();
});

window.addEventListener('resize', () => {
	
});

